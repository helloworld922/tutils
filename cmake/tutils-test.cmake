option(TUTILS_BUILD_TESTING "Build Tests" ON)

##
# @brief Adds google tests with given filters
# @param executable executable name
# @param extra_args extra arguments to pass to the executable
# @param ARGN test names, blank to add without any filters
function(ADD_GTESTS executable extra_args)
  set_target_properties(${executable}
    PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
    )
  if(ARGN)
    foreach(test_name ${ARGN})
      add_test(NAME "${test_name}" COMMAND ${executable} --gtest_filter=${test_name} ${extra_args})
    endforeach()
  else()
    add_test(NAME "${test_name}" COMMAND ${executable})
  endif()
endfunction()

if(TUTILS_BUILD_TESTING)
  enable_testing()

  add_subdirectory(${PROJECT_SOURCE_DIR}/test)
endif()
