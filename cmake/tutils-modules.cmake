# google test insists on supporting CMake 2.8.12
if(NOT DEFINED CMAKE_WARN_DEPRECATED)
  set(CMAKE_WARN_DEPRECATED Off CACHE INTERNAL "No deprecated warnings")
endif()
#set(CMAKE_WARN_DEPRECATED OFF CACHE BOOL "" FORCE)
# add google test subdir
add_subdirectory(deps/googletest EXCLUDE_FROM_ALL)

if(gtest_disable_pthreads)
  message(STATUS "gtest without pthreads")
  set(TUTILS_LIBRARIES gtest)
else()
  message(STATUS "gtest with pthreads")
  find_package(Threads REQUIRED)
  set(TUTILS_LIBRARIES gtest;${CMAKE_THREAD_LIBS_INIT})
endif()


