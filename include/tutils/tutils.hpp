#ifndef TUTILS_HPP
#define TUTILS_HPP

#include "gtest/gtest.h"
#include <iterator>
#include <algorithm>
#include <functional>
#include <string>
#include <cmath>
#include <sstream>
#include <limits>
#include <type_traits>

#include <iomanip>

// replace the default Google Test floating point near comparisons with
// more general ones
#undef EXPECT_NEAR
#undef ASSERT_NEAR

/**
 * @file tutils.hpp
 * @param Helpful utilities for writing tests.
 */
#if !defined(TUTILS_DOC) || defined(TUTILS_INTERNAL_DOCS)
namespace tutils
{
  namespace detail
  {
    template <class A, class B>
    std::string bop_format(A&& a, B&& b, const char* op)
    {
      std::stringstream s;
      s << std::setprecision(
             std::numeric_limits<typename std::decay<A>::type>::max_digits10)
        << ::testing::PrintToString(a) << ", " << ::testing::PrintToString(b);
      return s.str();
    }

    template <class Real>
    struct near_pred
    {
      Real tol;

      template <class R>
      near_pred(R&& tol) : tol(std::forward<R>(tol))
      {
      }

      near_pred(const near_pred&) = default;
      near_pred(near_pred&&) = default;

      template <class A, class B>
      bool operator()(const A& a, const B& b) const
      {
        using namespace std;
        auto diff = abs(a - b);
        return diff < tol;
      }

      template <class A, class B>
      std::string format(A&& a, B&& b) const
      {
        using namespace std;
        std::stringstream s;
        s << std::setprecision(
               std::numeric_limits<typename std::decay<A>::type>::max_digits10)
          << "abs(" << a << " - " << b << ") = " << abs(a - b);
        return s.str();
      }

      std::string format(const char* a, const char* b) const
      {
        using namespace std;
        std::stringstream s;
        s << std::setprecision(std::numeric_limits<decltype(tol)>::max_digits10)
          << "abs(" << a << " - " << b << ") < " << tol;
        return s.str();
      }
    };

    struct eq_pred
    {
      template <class A, class B>
      bool operator()(A&& a, B&& b) const
      {
        return a == b;
      }

      template <class A, class B>
      std::string format(A&& a, B&& b) const
      {
        return bop_format(std::forward<A>(a), std::forward<B>(b), "==");
      }
    };

    struct lt_pred
    {
      template <class A, class B>
      bool operator()(A&& a, B&& b) const
      {
        return a < b;
      }

      template <class A, class B>
      std::string format(A&& a, B&& b) const
      {
        return bop_format(std::forward<A>(a), std::forward<B>(b), "<");
      }
    };

    struct le_pred
    {
      template <class A, class B>
      bool operator()(A&& a, B&& b) const
      {
        return a <= b;
      }

      template <class A, class B>
      std::string format(A&& a, B&& b) const
      {
        return bop_format(std::forward<A>(a), std::forward<B>(b), "<=");
      }
    };

    struct gt_pred
    {
      template <class A, class B>
      bool operator()(A&& a, B&& b) const
      {
        return a > b;
      }

      template <class A, class B>
      std::string format(A&& a, B&& b) const
      {
        return bop_format(std::forward<A>(a), std::forward<B>(b), ">");
      }
    };

    struct ge_pred
    {
      template <class A, class B>
      bool operator()(A&& a, B&& b) const
      {
        return a >= b;
      }

      template <class A, class B>
      std::string format(A&& a, B&& b) const
      {
        return bop_format(std::forward<A>(a), std::forward<B>(b), ">=");
      }
    };

    template <class R1, class R2, class T>
    ::testing::AssertionResult pred_near_format(const char* expr1,
      const char* expr2, const char* abs_error_expr, R1&& val1, R2&& val2,
      T&& tol)
    {
      using namespace std;
      auto diff = abs(val1 - val2);
      if (diff <= tol)
      {
        return ::testing::AssertionSuccess();
      }
      ::testing::AssertionResult res = ::testing::AssertionFailure();
      res = res << "The difference between " << expr1 << " and " << expr2
                << " is " << diff << ", which exceeds " << abs_error_expr << ", where\n"
                << expr1 << " evaluates to " << val1 << ",\n"
                << expr2 << " evaluates to " << val2 << ", and\n"
                << abs_error_expr << " evaluates to " << tol << ".";

      return res;
    }

    template <class R1, class R2, class Pred>
    ::testing::AssertionResult pred_ranges(const char* str1, const char* str2,
      const char* str3, R1&& r1, R2&& r2, Pred&& pred)
    {
      using std::begin;
      using std::end;
      auto iter1 = begin(r1);
      auto iter2 = begin(r2);
      auto end1 = end(r1);
      auto end2 = end(r2);
      size_t i = 0;
      bool failure = false;
      ::testing::AssertionResult res = ::testing::AssertionFailure();
      for (; iter1 != end1 && iter2 != end2; ++iter1, ++iter2, ++i)
      {
        if (!pred(*iter1, *iter2))
        {
          if (!failure)
          {
            res = res << "expected: " << pred.format(str1, str2) << '\n';
          }
          res = res << "Mismatch at position " << i << ": "
                    << pred.format(*iter1, *iter2) << "\n";
          failure = true;
        }
      }
      if (iter1 == end1 && iter2 == end2)
      {
        if (!failure)
        {
          return ::testing::AssertionSuccess();
        }
        return res;
      }
      // length mismatch
      size_t j = i;
      i += std::distance(iter1, end1);
      j += std::distance(iter2, end2);
      return res << "expected length" << str1 << ": " << i << "\nactual length"
                 << str2 << ": " << j;
    }
  }
}

/**
 * @brief Asserts that for all elements in val1 and val2, pred(val1, val2)
 * returns true
 * @param val1
 * @param val2
 * @param pred binary predicate
 */
#define ASSERT_RANGE(val1, val2, pred)                                         \
  ASSERT_PRED_FORMAT3(tutils::detail::pred_ranges, (val1), (val2), (pred))

/**
 * @brief Expects that for all elements in val1 and val2, pred(val1, val2)
 * returns true
 * @param val1
 * @param val2
 * @param pred binary predicate
 */
#define EXPECT_RANGE(val1, val2, pred)                                         \
  ASSERT_PRED_FORMAT3(tutils::detail::pred_ranges, (val1), (val2), (pred))

/**
 * @brief Asserts that elements in val1 compare == to elements in val2.
 * @param val1
 * @param val2
 */
#define ASSERT_RANGE_EQ(val1, val2)                                            \
  ASSERT_PRED_FORMAT3(                                                         \
    tutils::detail::pred_ranges, (val1), (val2), tutils::detail::eq_pred())

/**
 * @brief Expects that elements in val1 compare == to elements in val2.
 * @param val1
 * @param val2
 */
#define EXPECT_RANGE_EQ(val1, val2)                                            \
  EXPECT_PRED_FORMAT3(                                                         \
    tutils::detail::pred_ranges, (val1), (val2), tutils::detail::eq_pred())

/**
 * @brief Asserts that elements in val1 compare < to elements in val2.
 * @param val1
 * @param val2
 */
#define ASSERT_RANGE_LT(val1, val2)                                            \
  ASSERT_PRED_FORMAT3(                                                         \
    tutils::detail::pred_ranges, (val1), (val2), tutils::detail::lt_pred())

/**
 * @brief Expects that elements in val1 compare < to elements in val2.
 * @param val1
 * @param val2
 */
#define EXPECT_RANGE_LT(val1, val2)                                            \
  EXPECT_PRED_FORMAT3(                                                         \
    tutils::detail::pred_ranges, (val1), (val2), tutils::detail::lt_pred())

/**
 * @brief Asserts that elements in val1 compare <= to elements in val2.
 * @param val1
 * @param val2
 */
#define ASSERT_RANGE_LE(val1, val2)                                            \
  ASSERT_PRED_FORMAT3(                                                         \
    tutils::detail::pred_ranges, (val1), (val2), tutils::detail::le_pred())

/**
 * @brief Expects that elements in val1 compare <= to elements in val2.
 * @param val1
 * @param val2
 */
#define EXPECT_RANGE_LE(val1, val2)                                            \
  EXPECT_PRED_FORMAT3(                                                         \
    tutils::detail::pred_ranges, (val1), (val2), tutils::detail::le_pred())

/**
 * @brief Asserts that elements in val1 compare > to elements in val2.
 * @param val1
 * @param val2
 */
#define ASSERT_RANGE_GT(val1, val2)                                            \
  ASSERT_PRED_FORMAT3(                                                         \
    tutils::detail::pred_ranges, (val1), (val2), tutils::detail::gt_pred())

/**
 * @brief Expects that elements in val1 compare > to elements in val2.
 * @param val1
 * @param val2
 */
#define EXPECT_RANGE_GT(val1, val2)                                            \
  EXPECT_PRED_FORMAT3(                                                         \
    tutils::detail::pred_ranges, (val1), (val2), tutils::detail::gt_pred())

/**
 * @brief Asserts that elements in val1 compare >= to elements in val2.
 * @param val1
 * @param val2
 */
#define ASSERT_RANGE_GE(val1, val2)                                            \
  ASSERT_PRED_FORMAT3(                                                         \
    tutils::detail::pred_ranges, (val1), (val2), tutils::detail::ge_pred())

/**
 * @brief Expects that elements in val1 compare >= to elements in val2.
 * @param val1
 * @param val2
 */
#define EXPECT_RANGE_GE(val1, val2)                                            \
  EXPECT_PRED_FORMAT3(                                                         \
    tutils::detail::pred_ranges, (val1), (val2), tutils::detail::ge_pred())

/**
 * @brief Asserts that elements in val1 compare approximately equal to elements
 * in val2.
 * @param val1
 * @param val2
 * @param tol absolute tolerance
 */
#define ASSERT_RANGE_NEAR(val1, val2, tol)                                     \
  ASSERT_PRED_FORMAT3(tutils::detail::pred_ranges, (val1), (val2),             \
    tutils::detail::near_pred<typename std::decay<decltype(tol)>::type>(tol))

/**
 * @brief Expects that elements in val1 compare approximately equal to elements
 * in val2.
 * @param val1
 * @param val2
 * @param tol absolute tolerance
 */
#define EXPECT_RANGE_NEAR(val1, val2, tol)                                     \
  EXPECT_PRED_FORMAT3(tutils::detail::pred_ranges, (val1), (val2),             \
    tutils::detail::near_pred<typename std::decay<decltype(tol)>::type>(tol))

/**
 * @brief Asserts that elements in val1 compare approximately equal to elements
 * in val2.
 * @param val1
 * @param val2
 * @param tol absolute tolerance
 */
#define ASSERT_NEAR(val1, val2, tol)                                           \
  ASSERT_PRED_FORMAT3(tutils::detail::pred_near_format, (val1), (val2), (tol))

/**
 * @brief Expects that elements in val1 compare approximately equal to elements
 * in val2.
 * @param val1
 * @param val2
 * @param tol absolute tolerance
 */
#define EXPECT_NEAR(val1, val2, tol)                                           \
  EXPECT_PRED_FORMAT3(tutils::detail::pred_near_format, (val1), (val2), (tol))
#endif

struct any_tests_run : public ::testing::EmptyTestEventListener
{
  bool any_run_ = false;

  // Called before a test starts.
  void OnTestStart(const ::testing::TestInfo& test_info) override
  {
    any_run_ = true;
  }

  void OnTestPartResult(const ::testing::TestPartResult& test_part_result) override
  {
  }

  void OnTestEnd(const ::testing::TestInfo& test_info) override
  {
  }
};

void init_tutils(int* argc, char** argv);

int run_tutils();

#endif
