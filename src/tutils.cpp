#include "tutils/tutils.hpp"

static any_tests_run* tutils_instance_;

::testing::AssertionResult any_run()
{
  if(tutils_instance_->any_run_)
  {
    return ::testing::AssertionSuccess();
  }
  else
  {
    return ::testing::AssertionFailure() << "no tests were run";
  }
}

void init_tutils(int* argc, char** argv)
{
  ::testing::InitGoogleTest(argc, argv);
  ::testing::TestEventListeners& listeners =
      ::testing::UnitTest::GetInstance()->listeners();
  // Adds a listener to the end.  Google Test takes the ownership.
  tutils_instance_ = new any_tests_run();
  listeners.Append(tutils_instance_);
}

int run_tutils()
{
  auto res = RUN_ALL_TESTS();
  EXPECT_TRUE(any_run());
  return res | !tutils_instance_->any_run_;
}
