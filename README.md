# About

Tutils ("Tootles") is a small test utilities library extending the Google Test framework.

# Install

See [this page](doc/install.md) for build/install instructions.

# Contributing

TODO

# License

tutils is licensed under the [Boost license](LICENSE.md).

